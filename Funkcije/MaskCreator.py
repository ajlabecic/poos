from skimage import draw
from skimage import io
import numpy as np
import urllib.request
import json
import os

dirname = os.path.dirname(__file__)

def create_masks(path_to_annotation, path_to_images, path_to_masks):
    if (not os.path.isdir(os.path.join(dirname,path_to_images))):
        print("Folder {} ne postoji.".format(path_to_images))
        return
    if (not os.path.isdir(os.path.join(dirname,path_to_masks))):
        print("Folder {} ne postoji.".format(path_to_masks))
        return
    if (not os.path.exists(os.path.join(dirname,path_to_annotation))):
        print("Folder {} ne postoji.".format(path_to_annotation))
        return
    
    for i in range(0,10):
        if not os.path.isdir(os.path.join(dirname, path_to_images) + "/" + str(i)):
            os.makedirs(os.path.join(dirname,path_to_images) + "/" + str(i))
        if not os.path.isdir(os.path.join(dirname, path_to_masks) + "/" + str(i)):
            os.makedirs(os.path.join(dirname,path_to_masks) + "/" + str(i))
    
    f = open(os.path.join(dirname,path_to_annotation))
    lines = f.readlines()
    data = []
    for line in lines:
        d = json.loads(line)
        data.append(d)
    c = 0
    for d in data:
        if "TEST" not in d['content']:
            annotations = d['annotation']        
            for annotation in annotations:
                label = str(annotation['label'])
                label = label[2:3]
                urllib.request.urlretrieve(d['content'], os.path.join(dirname,path_to_images) + "/" + str(label) + "/" + str(c) + ".jpg")
                if label != '':
                    points = annotation['points']
                    height = annotation['imageHeight']
                    width = annotation['imageWidth']
                    x_coordinates = []
                    y_coordinates = []
                    for point in points:
                        x_coordinates.append(point[0] * width)
                        y_coordinates.append(point[1] * height)
                    shape = (height, width)
                    mask = np.zeros((height, width,3))
                    row_coordinates, col_coordinates = draw.polygon(y_coordinates, x_coordinates, shape)
                    mask[row_coordinates, col_coordinates] = 1
                            
                    io.imsave(os.path.join(dirname,path_to_masks) + "/" + str(label) + "/" + str(c) + ".jpg", mask)
        c+=1

# unijeti putanje
# fajl u kome se nalaze anotacije
path_to_annotation = '..\\Anotacije\\POOS_NOVI.json'
# folder za spremanje originalnih slika
path_to_images = '..\\Histogram'
# folder za spremanje maski
path_to_masks = '..\\Maske2\\Maske'
create_masks(path_to_annotation, path_to_images, path_to_masks)



