# -*- coding: utf-8 -*-
from PIL import Image
from PIL import  ImageChops
import os
from os import listdir
from os.path import isfile, join
import numpy as np
from skimage import io
import cv2
from save import save_image
from pyefd import elliptic_fourier_descriptors
from pyefd import plot_efd
import matplotlib
from matplotlib import pyplot as plt
from skimage.feature import hog
from skimage import data, exposure

from sklearn.neighbors import KNeighborsClassifier

dirname = os.path.dirname(__file__)


#def save_image(path, image):
 #   io.imsave(path,image)


def masks_on_images(path_to_masks, path_to_images, path_to_results):
    path_to_masks = os.path.join(dirname, path_to_masks)
    path_to_images = os.path.join(dirname, path_to_results)
    path_to_results = os.path.join(dirname, path_to_results)
    images = []
    masks = []
    AR = []
    LAB = []
    for i in range(0,10):
        images.append([f for f in listdir(path_to_images + "/" + str(i)) if isfile(join(path_to_images + "/" + str(i), f))])
        masks.append([f for f in listdir(path_to_masks + "/" + str(i)) if isfile(join(path_to_masks + "/" + str(i), f))])
        if not os.path.isdir(path_to_results + "/" + str(i)):
            os.makedirs(path_to_results + "/" + str(i))
    lower = np.array([0, 30, 60], dtype = "uint8")
    upper = np.array([200, 150, 255], dtype = "uint8")
    for i in range(0,10):
        images_for_class_i = images[i]
        print(images_for_class_i)
        masks_for_class_i = masks[i]
        length = len(images_for_class_i)
        for j in range(0, length):
            image = cv2.imread(path_to_images + "/" + str(i) + "/" +images_for_class_i[j])
            fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
                                cells_per_block=(1, 1), visualize=True, multichannel=True)
            
            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)
            
            ax1.axis('off')
            ax1.imshow(image, cmap=plt.cm.gray)
            ax1.set_title('Input image')
            
            # Rescale histogram for better display
            hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 10))
            
            ax2.axis('off')
            ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
            ax2.set_title('Histogram of Oriented Gradients')
            #plt.show()
            AR.append(hog_image)
            LAB.append(i)
        return [AR, LAB]
# unijeti putanje 
# folder za učitavanje originalnih slika
path_to_images = '..\\Maske\\OriginalneSlike'
# folder za učitavanje maski
path_to_masks = '..\\Maske\\Maske'
#folder za spremanje rezultata primjene maski na slike
path_to_results = '..\\Maske\\MaskiraneSlike'
a = masks_on_images(path_to_masks, path_to_images, path_to_results)
neigh = KNeighborsClassifier(n_neighbors=3)
#neigh.fit(a[0], a[1]) 
path_to_images2 = os.path.join(dirname, '..\\Slikezazavrsnotestiranje')
if not os.path.isdir(path_to_images2):
    print("KKK")
print(path_to_images2)
print(a[0][0][4])
#img = cv2.imread("C:\\Users\\xy\\Desktop\\poos\\Slikezazavršnotestiranje\\TEST_IMG_5256.JPG",0)
image = cv2.imread(path_to_images2 + "/TEST_IMG_4228.JPG")
#image = cv2.imread(path_to_images + "/" + str(i) + "/" +images_for_class_i[j])
fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
                                cells_per_block=(1, 1), visualize=True, multichannel=True)
hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 10))
test = []
test.append(hog_image_rescaled)
#print(neigh.predict(test))
            