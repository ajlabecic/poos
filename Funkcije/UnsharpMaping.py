from scipy import misc
from scipy import ndimage
from scipy import signal
import cv2
import glob
from save import save_image

for i in range(0,10):
    for filename in glob.glob('../FiltriraneSlike/'+str(i)+'/*.JPG'):
        im=misc.imread(filename)

        # Calculate the Laplacian
        lap = cv2.Laplacian(im,cv2.CV_64F)

        # Calculate the sharpened image
        sharp = im - 0.3*lap 
        split = filename.split('/')

        save_image( "../UnsharpMaskedSlike/"+str(i)+"/"+split[len(split)-1], sharp)
 