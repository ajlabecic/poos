import numpy as np
import os
import scipy.ndimage
from skimage.feature import hog, canny
from skimage import data, color, exposure
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib
import cv2
dirname = os.path.dirname(__file__)

def get_features(path_to_images):
    path_to_images= os.path.join(dirname, path_to_images)
    images = []
    features_list = []
    features_label = []
    for i in range(0,10):
        images.append([f for f in os.listdir(path_to_images + "/" + str(i)) if os.path.isfile(os.path.join(path_to_images + "/" + str(i), f))])
    for i in range(0,10):
        images_i = images[i]
        length = len(images_i)
        for j in range(0, length):
            image = scipy.misc.imread(path_to_images + "/" + str(i) + "/" + images_i[j])
            #image = object_detection(image)
            image = color.rgb2gray(image)
            df= hog(image, orientations=8, pixels_per_cell=(15,15), cells_per_block=(5,5))
            features_list.append(df)
            features_label.append(i)
    features  = np.array(features_list, 'float64')
    return (features, features_label)

def train(features, labels):
    knn = KNeighborsClassifier(n_neighbors=6) #6 #7traintest2
    knn.fit(features, labels) 
    return knn

def export(knn):
    joblib.dump(knn, 'knn_model.pkl')

path_to_orig = "..\\Maske\\OriginalneSlike"
path_to_mask = "..\\Maske2\\MaskiraneSlike"
path_to_train = "..\\TrainTest\\train"
path_to_test = "..\\TrainTest\\test"
features, labels = get_features(path_to_train)
features_test, labels_test = get_features(path_to_test)


knn = train(features, labels)
print(knn.score(features_test, labels_test))
export(knn)