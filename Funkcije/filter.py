from scipy import misc
from scipy import ndimage
from scipy import signal
import matplotlib.pyplot as plt
import glob
from save import save_image

for i in range(0,10):
    for filename in glob.glob('../Maske/originalneSlike/'+str(i)+'/*.JPG'):
        im=misc.imread(filename)
        im_med  = ndimage.median_filter(im, 1)

        split = filename.split('/')

        save_image( "../FiltriraneSlike/"+str(i)+"/"+split[len(split)-1], im_med)
