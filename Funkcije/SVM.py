import numpy as np
import os
import scipy.ndimage
from skimage.feature import hog
from skimage import data, color, exposure
from sklearn.model_selection import  train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib
from sklearn import svm
dirname = os.path.dirname(__file__)



def get_features(path_to_images):
    path_to_images= os.path.join(dirname, path_to_images)
    images = []
    features_list = []
    features_label = []
    for i in range(0,10):
        images.append([f for f in os.listdir(path_to_images + "/" + str(i)) if os.path.isfile(os.path.join(path_to_images + "/" + str(i), f))])
    for i in range(0,10):
        images_i = images[i]
        length = len(images_i)
        for j in range(0, length):
            image = scipy.misc.imread(path_to_images + "/" + str(i) + "/" + images_i[j])
            image = color.rgb2gray(image)
            df= hog(image, orientations=8, pixels_per_cell=(15,15), cells_per_block=(5,5))
            features_list.append(df)
            features_label.append(i)
    features  = np.array(features_list, 'float64')
    return (features, features_label)

path_to_orig = "..\\Maske\\OriginalneSlike"
path_to_mask = "..\\Maske\\MaskiraneSlike"

path_to_train = "..\\TrainTest\\train"
path_to_test = "..\\TrainTest\\test"
features, labels = get_features(path_to_train)
f_t, l_t = get_features(path_to_test)
X_train, X_test, y_train, y_test = train_test_split(features, labels)
clf = svm.SVC(kernel="poly", probability = True)
clf.fit(features, labels)
print(clf.score(f_t, l_t))
#joblib.dump(clf, 'svm_model.pkl')