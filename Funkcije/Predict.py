import numpy as np
import os
import scipy.ndimage
from skimage.feature import hog
from skimage import data, color, exposure
from sklearn.model_selection import  train_test_split
import cv2
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 

knn = joblib.load('knn_model.pkl')

dirname = os.path.dirname(__file__)

def object_detection(image):
    lower = np.array([0, 30, 60], dtype = "uint8")
    upper = np.array([200, 150, 255], dtype = "uint8")
    converted = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    skinMask = cv2.inRange(converted, lower, upper)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2,2))
    skinMask = cv2.erode(skinMask, kernel, iterations = 1)
    skinMask = cv2.dilate(skinMask, kernel, iterations = 1)
    skinMask = cv2.GaussianBlur(skinMask, (3, 3), 0)
    skin = cv2.bitwise_and(image, image, mask = skinMask)
    for i in range(0,100):
        for j in range(0,100):
            for k in range(0,3):
                if skin[i,j,k] >240:
                    skin[i,j,k] = 0
        
    return skin

def convert2gray(image):
    return color.rgb2gray(image)


def feature_extraction(image):
    image = object_detection(image)
    #cv2.imshow("image",image)
    return hog(convert2gray(image), orientations=8, pixels_per_cell=(15, 15), cells_per_block=(5, 5))

def predict(df):
    predict = knn.predict(df.reshape(1,-1))[0]
    predict_proba = knn.predict_proba(df.reshape(1,-1))
    return predict, predict_proba[0]

def detect(images):
    image = []
    for i in range(0, len(images[0])):
        print(images[0][i])
        image.append(scipy.misc.imread(path_to_images + "/" + images[0][i]))
        hogs = list(map(lambda x: feature_extraction(x), image))
        predictions = list(map(lambda x: predict(x), hogs))
        result(images[0][i], predictions[0][0])
        image = []

def result(name, prediction):
    img = Image.open(path_to_images + "/" + name)
    draw = ImageDraw.Draw(img)
    draw.text((0, 0),str(prediction),(255,255,255))
    img.save(path_to_images + "/rezultat/" + name)


def main(path_to_images):
    images = []
    images.append([f for f in os.listdir(path_to_images) if os.path.isfile(os.path.join(path_to_images, f))])
    detect(images)

path_to_images = os.path.join(dirname, "..\\Slikezazavrsnotestiranje")
if __name__ == '__main__':
    main(path_to_images)
#object_detection(scipy.misc.imread(path_to_images + "/" + images[0][4]))