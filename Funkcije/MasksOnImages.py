# -*- coding: utf-8 -*-
from PIL import Image
from PIL import  ImageChops
import os
from os import listdir
from os.path import isfile, join
import numpy as np
from skimage import io
from save import save_image

dirname = os.path.dirname(__file__)

#def save_image(path, image):
 #   io.imsave(path,image)


def masks_on_images(path_to_masks, path_to_images, path_to_results):
    path_to_masks = os.path.join(dirname, path_to_masks)
    path_to_images = os.path.join(dirname, path_to_images)
    path_to_results = os.path.join(dirname, path_to_results)
    images = []
    masks = []
    for i in range(0,10):
        images.append([f for f in listdir(path_to_images + "/" + str(i)) if isfile(join(path_to_images + "/" + str(i), f))])
        masks.append([f for f in listdir(path_to_masks + "/" + str(i)) if isfile(join(path_to_masks + "/" + str(i), f))])
        if not os.path.isdir(path_to_results + "/" + str(i)):
            os.makedirs(path_to_results + "/" + str(i))
    
    for i in range(0,10):
        images_for_class_i = images[i]
        masks_for_class_i = masks[i]
        length = len(images_for_class_i)
        for j in range(0, length):
            image = Image.open(path_to_images + "/" + str(i) + "/" +images_for_class_i[j])
            mask = Image.open(path_to_masks + "/" + str(i) + "/" + masks_for_class_i[j])
            image.load()
            mask.load()
            result = ImageChops.multiply(image,mask)
            result = np.asarray(result, dtype="int32")
            
            save_image(path_to_results + "/"+ str(i) + "/" + images_for_class_i[j], result)
            
# unijeti putanje 
# folder za učitavanje originalnih slika
path_to_images = '..\\Histogram'
# folder za učitavanje maski
path_to_masks = '..\\Maske2\\Maske'
#folder za spremanje rezultata primjene maski na slike
path_to_results = '..\\Maske2\\MaskiraneSlike'
masks_on_images(path_to_masks, path_to_images, path_to_results)