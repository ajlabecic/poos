import numpy as np
import os
import scipy.ndimage
from skimage.feature import hog, canny
from skimage import data, color, exposure
from sklearn.externals import joblib
import cv2
from sklearn.metrics import confusion_matrix


knn = joblib.load('knn_model.pkl')
dirname = os.path.dirname(__file__)

def get_features(path_to_images):
    path_to_images= os.path.join(dirname, path_to_images)
    images = []
    features_list = []
    features_label = []
    for i in range(0,10):
        images.append([f for f in os.listdir(path_to_images + "/" + str(i)) if os.path.isfile(os.path.join(path_to_images + "/" + str(i), f))])
    for i in range(0,10):
        images_i = images[i]
        length = len(images_i)
        for j in range(0, length):
            image = scipy.misc.imread(path_to_images + "/" + str(i) + "/" + images_i[j])
            #image = object_detection(image)
            image = color.rgb2gray(image)
            df= hog(image, orientations=8, pixels_per_cell=(15,15), cells_per_block=(5,5))
            features_list.append(df)
            features_label.append(i)
    features  = np.array(features_list, 'float64')
    return (features, features_label)

def predict(df):
    predict = knn.predict(df.reshape(1, -1))[0]
    return predict


def accuracy(indeks, mat):
    TP = mat[indeks][indeks]
    TN = 0
    ALL = 0
    for i in range(len(mat[0])):
        for j in range(len(mat[0])):
            ALL += mat[i][j]
            if i == indeks or j == indeks:
                continue
            TN += mat[i][j]
    return (TP+TN)/ALL

def sensitivity(indeks, mat):
    TP = mat[indeks][indeks]
    FN = 0
    for i in range(len(mat[0])):
        if i != indeks:
            FN += mat[indeks][i]
    return TP/(TP+FN)

def specificity(indeks, mat):
    TN = 0
    FP = 0
    for i in range(len(mat[0])):
        if i == indeks:
            continue
        for j in range(len(mat[0])):
            if j == indeks:
                continue
            TN += mat[i][j]
        FP += mat[i][indeks]
    return TN/(TN+FP)


path_to_test = "..\\TrainTest\\test"
features_test, labels_test = get_features(path_to_test)
model_score = knn.score(features_test, labels_test)
print("ACC={}".format(model_score))

ukupno = np.zeros(10)
TP = np.zeros(10)
predicted = []
for i in range(len(features_test)):
    predict1 = predict(features_test[i])
    predicted.append(int(predict1))

confusion = confusion_matrix(labels_test, predicted)
#print(confusion)
for i in range(len(confusion)):
    print("Class " + str(i) + ": ", end=" ")
    print("sensitivity: " + str(sensitivity(i, confusion)) + ", specificity: " + str(specificity(i, confusion)) + ", accuracy: " + str(accuracy(i, confusion)))
