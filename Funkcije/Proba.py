# -*- coding: utf-8 -*-
from PIL import Image
from PIL import  ImageChops
import os
from os import listdir
from os.path import isfile, join
import numpy as np
from skimage import io
import cv2
from save import save_image
from pyefd import elliptic_fourier_descriptors
from pyefd import plot_efd
import matplotlib
from sklearn.svm import SVC 
from sklearn.neighbors import KNeighborsClassifier


dirname = os.path.dirname(__file__)

#def save_image(path, image):
 #   io.imsave(path,image)

def reconstruct(descriptors, degree):
    """ reconstruct(descriptors, degree) attempts to reconstruct the image
    using the first [degree] descriptors of descriptors"""
    # truncate the long list of descriptors to certain length
    descriptor_in_use = truncate_descriptor(descriptors, degree)
    contour_reconstruct = np.fft.ifft(descriptor_in_use)
    contour_reconstruct = np.array(
        [contour_reconstruct.real, contour_reconstruct.imag])
    contour_reconstruct = np.transpose(contour_reconstruct)
    contour_reconstruct = np.expand_dims(contour_reconstruct, axis=1)
    # make positive
    if contour_reconstruct.min() < 0:
        contour_reconstruct -= contour_reconstruct.min()
    # normalization
    contour_reconstruct *= 800 / contour_reconstruct.max()
    # type cast to int32
    contour_reconstruct = contour_reconstruct.astype(np.int32, copy=False)
    black = np.zeros((800, 800), np.uint8)
    # draw and visualize
    cv2.drawContours(black, contour_reconstruct, -1, 255, thickness=2)
    #cv2.imshow("black", black)
    #cv2.waitKey(1000)
    #cv2.imwrite("reconstruct_result.jpg", black)
    #cv2.destroyAllWindows()
    return descriptor_in_use

def truncate_descriptor(descriptors, degree):
    """this function truncates an unshifted fourier descriptor array
    and returns one also unshifted"""
    descriptors = np.fft.fftshift(descriptors)
    center_index = len(descriptors) // 2
    descriptors = descriptors[center_index - degree // 2:center_index + degree // 2]
    descriptors = np.fft.ifftshift(descriptors)
    return descriptors


def masks_on_images(path_to_masks, path_to_images, path_to_results):
    path_to_masks = os.path.join(dirname, path_to_masks)
    path_to_images = os.path.join(dirname, path_to_images)
    path_to_results = os.path.join(dirname, path_to_results)
    array = []
    labels = []
    images = []
    masks = []
    for i in range(0,10):
        images.append([f for f in listdir(path_to_images + "/" + str(i)) if isfile(join(path_to_images + "/" + str(i), f))])
        masks.append([f for f in listdir(path_to_masks + "/" + str(i)) if isfile(join(path_to_masks + "/" + str(i), f))])
        if not os.path.isdir(path_to_results + "/" + str(i)):
            os.makedirs(path_to_results + "/" + str(i))
    lower = np.array([0, 30, 60], dtype = "uint8")
    upper = np.array([200, 150, 255], dtype = "uint8")
    for i in range(5,10):
        images_for_class_i = images[i]
        print(images_for_class_i)
        masks_for_class_i = masks[i]
        length = len(images_for_class_i)
        for j in range(0, length):
            print(j)
            image = cv2.imread(path_to_images + "/" + str(i) + "/" +images_for_class_i[j])
            #image =np.asarray(Image.open(path_to_images + "/" + str(i) + "/" +images_for_class_i[j]))
            imgray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
            #ret,thresh = cv2.threshold(imgray,127,255,0)
            thresh = cv2.adaptiveThreshold(imgray, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
	cv2.THRESH_BINARY_INV, 11, 7)
            kk,contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
            cnts = sorted(contours, key = cv2.contourArea, reverse = True)[0]
            #outline = np.zeros(image.shape, dtype = "uint8")
            #cv2.drawContours(outline, [cnts], -1, 255, -1)
            #array.append(cnts)
            #labels.append(i)
            #if j == 0:
            #cv2.imshow("images", np.hstack([outline]))
            #M = cv2.moments(cnts)
            
            
            contour_array = contours[0][:, 0, :]
            contour_complex = np.empty(contour_array.shape[:-1], dtype=complex)
            contour_complex.real = contour_array[:, 0]
            contour_complex.imag = contour_array[:, 1]
            fourier_result = np.fft.fft(contour_complex)
            contour_reconstruct = reconstruct(fourier_result, 3)
            array.append(contour_reconstruct)
            labels.append(i)
            
            #cv2.imshow("images", np.hstack([image]))
            #for cnt in contours:
    # Find the coefficients of all contours
                #coeffs.append(elliptic_fourier_descriptors(np.squeeze(cnt), order=10))
            #print(len(np.asarray(coeffs)))
           # plot_efd(np.asarray(coeffs))
            #print(coeffs)
            #print(type(image[0][0][0]))
            #print(image)
            #break
            '''
            converted = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
            skinMask = cv2.inRange(converted, lower, upper)
            print(images_for_class_i[j])
            '''
            '''
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2,2))
            skinMask = cv2.erode(skinMask, kernel, iterations = 1)
            skinMask = cv2.dilate(skinMask, kernel, iterations = 1)
       	    skinMask = cv2.GaussianBlur(skinMask, (3, 3), 0)
       	    skin = cv2.bitwise_and(image, image, mask = skinMask)
       	    '''
       	   # cv2.imshow("images", np.hstack([image, skin]))
       	    #dst = np.ndarray()
       	    #dst = cv2.ximgproc.fourierDescriptor(thresh)
       	    #print(dst)
       	    #dst = cv2.ximgproc.createContourFitting()#(image,200)
 
	# show the skin in the image along with the ma
            
            '''
            mask = Image.open(path_to_masks + "/" + str(i) + "/" + masks_for_class_i[j])
            image.load()
            mask.load()
            result = ImageChops.multiply(image,mask)
            result = np.asarray(result, dtype="int32")
            
            save_image(path_to_results + "/"+ str(i) + "/" + images_for_class_i[j], result)
        '''
    return [array, labels]
# unijeti putanje 
# folder za učitavanje originalnih slika
path_to_images = '..\\Maske\\OriginalneSlike'
# folder za učitavanje maski
path_to_masks = '..\\Maske\\Maske'
#folder za spremanje rezultata primjene maski na slike
path_to_results = '..\\Maske\\MaskiraneSlike'
a = masks_on_images(path_to_masks, path_to_images, path_to_results)
svclassifier = SVC(kernel='rbf')
print((a[1]))
#print((a[0]))
ar = []
niz= []
for i in range(0, len(a[0])):
    ar.append(a[0][0])
#print(ar)
#svclassifier.fit(ar,a[1]) 
neigh = KNeighborsClassifier(n_neighbors=3)
neigh.fit(ar, a[1]) 
path_to_images2 = os.path.join(dirname, '..\\Slikezazavršnotestiranje')
if not os.path.isdir(path_to_images2):
    print("KKK")
print(path_to_images2)
#img = cv2.imread("C:\\Users\\xy\\Desktop\\poos\\Slikezazavršnotestiranje\\TEST_IMG_5256.JPG",0)
img = cv2.imread("C:\\Users\\xy\\Desktop\\poos\\Maske\\OriginalneSlike\\7\\144.jpg")
img2 = cv2.imread("C:\\Users\\xy\\Desktop\\poos\\Maske\\OriginalneSlike\\7\\145.jpg",0)
test = []

#print(img)
imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            #ret,thresh = cv2.threshold(imgray,127,255,0)
thresh = cv2.adaptiveThreshold(imgray, 255, cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY_INV, 11, 7)
kk,contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
cnts = sorted(contours, key = cv2.contourArea, reverse = True)[0]
slika = []
#print(len(cnts))
contour_array = contours[0][:, 0, :]
contour_complex = np.empty(contour_array.shape[:-1], dtype=complex)
contour_complex.real = contour_array[:, 0]
contour_complex.imag = contour_array[:, 1]
fourier_result = np.fft.fft(contour_complex)
contour_reconstruct = reconstruct(fourier_result, 3)
test.append(contour_reconstruct)
#print(svclassifier.predict(test))
print(len(test[0]))
#print(neigh.predict(test))
