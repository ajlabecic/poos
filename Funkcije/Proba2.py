# -*- coding: utf-8 -*-
from PIL import Image
from PIL import  ImageChops
import os
from os import listdir
from os.path import isfile, join
import numpy as np
from skimage import io
import cv2
from save import save_image
from pyefd import elliptic_fourier_descriptors
from pyefd import plot_efd
import matplotlib
from sklearn.svm import SVC 
from sklearn.neighbors import KNeighborsClassifier

from matplotlib import pyplot as plt
dirname = os.path.dirname(__file__)


def masks_on_images(path_to_masks, path_to_images, path_to_results):
    path_to_masks = os.path.join(dirname, path_to_masks)
    path_to_images = os.path.join(dirname, path_to_images)
    path_to_results = os.path.join(dirname, path_to_results)
    images = []
    masks = []
    array = []
    labels = []
    for i in range(0,10):
        images.append([f for f in listdir(path_to_images + "/" + str(i)) if isfile(join(path_to_images + "/" + str(i), f))])
        masks.append([f for f in listdir(path_to_masks + "/" + str(i)) if isfile(join(path_to_masks + "/" + str(i), f))])
        if not os.path.isdir(path_to_results + "/" + str(i)):
            os.makedirs(path_to_results + "/" + str(i))
    lower = np.array([0, 30, 60], dtype = "uint8")
    upper = np.array([200, 150, 255], dtype = "uint8")
    for i in range(0,10):
        images_for_class_i = images[i]
        masks_for_class_i = masks[i]
        length = len(images_for_class_i)
        for j in range(0, length):
            img = cv2.imread(path_to_images + "/" + str(i) + "/" +images_for_class_i[j],0)
            edges = cv2.Canny(img,100,200)
            plt.subplot(121),plt.imshow(img,cmap = 'gray')
            plt.title('Original Image'), plt.xticks([]), plt.yticks([])
            plt.subplot(122),plt.imshow(edges,cmap = 'gray')
            plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
            
            #plt.show()
            #image =np.asarray(Image.open(path_to_images + "/" + str(i) + "/" +images_for_class_i[j]))
            array.append(edges[0])
            labels.append(i)
          
    return [array, labels]
# unijeti putanje 
# folder za učitavanje originalnih slika
path_to_images = '..\\Maske\\OriginalneSlike'
# folder za učitavanje maski
path_to_masks = '..\\Maske\\Maske'
#folder za spremanje rezultata primjene maski na slike
path_to_results = '..\\Maske\\MaskiraneSlike'
a = masks_on_images(path_to_masks, path_to_images, path_to_results)
#svclassifier = SVC(kernel='rbf')
neigh = KNeighborsClassifier(n_neighbors=3)
neigh.fit(a[0], a[1]) 
#print(len(a[1]))
#print(len(a[0]))
#svclassifier.fit(a[0],a[1]) 
path_to_images2 = os.path.join(dirname, '..\\Slikezazavrsnotestiranje')
if not os.path.isdir(path_to_images2):
    print("KKK")
print(path_to_images2)
#img = cv2.imread("C:\\Users\\xy\\Desktop\\poos\\Slikezazavršnotestiranje\\TEST_IMG_5256.JPG",0)
img = cv2.imread(path_to_images2 + "/TEST_IMG_4228.JPG",0)
img2 = cv2.imread("C:\\Users\\xy\\Desktop\\poos\\Maske\\OriginalneSlike\\9\\185.jpg",0)
test = []

#print(img)
edges = cv2.Canny(img,100,200)
edges2 = cv2.Canny(img2,100,200)
test.append(edges[0])
test.append(edges2[0])
print(neigh.predict(test))
