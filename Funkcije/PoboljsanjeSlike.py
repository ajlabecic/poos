# -*- coding: cp1252 -*-
import imageio
import glob
import os
import numpy as np
from PIL import ImageFilter
from PIL import Image, ImageEnhance
from save import save_image

def normalizeRed(intensity):
    iI = intensity
    minI = 86
    maxI = 230
    minO = 0
    maxO = 255

    iO = (iI - minI) * (((maxO - minO) / (maxI - minI)) + minO)
    return iO

def normalizeGreen(intensity):
    iI = intensity
    minI = 90
    maxI = 225
    minO = 0
    maxO = 255

    iO = (iI - minI) * (((maxO - minO) / (maxI - minI)) + minO)
    return iO

def normalizeBlue(intensity):
    iI = intensity
    minI = 100
    maxI = 210
    minO = 0
    maxO = 255

    iO = (iI - minI) * (((maxO - minO) / (maxI - minI)) + minO)
    return iO

def contrast_all(imageObject):
    multiBands = imageObject.split()

    # contrast stretching primijenjen na svaku boju posebno
    normalizedRedBand = multiBands[0].point(normalizeRed)
    normalizedGreenBand = multiBands[1].point(normalizeGreen)
    normalizedBlueBand = multiBands[2].point(normalizeBlue)

    normalizedImage = Image.merge("RGB", (normalizedRedBand, normalizedGreenBand, normalizedBlueBand))
    return normalizedImage

def brightness(imageObject):
    enhancer = ImageEnhance.Brightness(imageObject)
    enhanced_im = enhancer.enhance(1.2)
    return enhanced_im

def contrast_edge():
    for i in range(0, 10):
        for filename in glob.glob('../Maske/OriginalneSlike/' + str(i) + '/*.JPG'):
            #im = imageio.imread(filename)
            directory = '../PoboljsaneSlike/' + str(i)

            if not os.path.exists(directory):
                os.makedirs(directory)
            split = filename.split('/')

            imageObject = Image.open(filename)

            # ***************** PRIMJENA FILTERA EDGE_ENHANCE ZA KONTRAST RUBOVA ******************

            # edge enhancement filter
            edgeEnahnced = imageObject.filter(ImageFilter.EDGE_ENHANCE)

            # increased edge enhancement filter
            #ne koristi se, ali za naglasenije rubove se moze upotrijebiti sljedeca linija
            #moreEdgeEnahnced = imageObject.filter(ImageFilter.EDGE_ENHANCE_MORE)


            # ******************* PRIMJENA DRUGOG KONTRASTA ********************
            imageObject = contrast_all(edgeEnahnced)

            # ************************ POBOLJ�ANJE OSVIJETLJENOSTI ***************
            imageObject = brightness(imageObject)

            save_image(directory + '/' + split[len(split) - 1], np.asarray(imageObject, dtype="uint8"))

contrast_edge()